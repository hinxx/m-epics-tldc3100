#!../../bin/linux-x86_64/tlDC3100DemoApp

## You may have to change tlDC3100DemoApp to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/tlDC3100Demo.dbd"
tlDC3100Demo_registerRecordDeviceDriver pdbbase

tlDC3100Config("DC3100", 0x1313, 0x8060, "M00417186")
asynSetTraceIOMask("DC3100",0,0xff)
#asynSetTraceMask("DC3100",0,0xff)

# Load record instances
dbLoadRecords("$(DC3100)/db/tlDC3100.template","P=DC3100:,R=,PORT=DC3100,ADDR=0,TIMEOUT=1")
dbLoadRecords("$(ASYN)/db/asynRecord.db","P=DC3100:,R=asyn,PORT=DC3100,ADDR=0,OMAX=100,IMAX=100")

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncxxx,"user=hinxxHost"
