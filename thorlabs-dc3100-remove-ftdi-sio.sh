#!/bin/bash

logger "Script $@ started.."
dev="$(basename $1):1.0"
drv="/sys/$1/$dev/driver"
logger "Will remove device $dev from driver $drv"
echo "$dev" > $drv/unbind
sleep 1
logger "Removed device $dev from driver $drv"
logger "Script $@ finished.."
