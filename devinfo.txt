
Using FTDI D2XX driver

EEPROM read test utility:

bi@bipc101 ~/dc3100/release/examples/EEPROM/read $ sudo ./read 
Library version = 0x10306
Opening port 0
FT_Open succeeded.  Handle is 0x10ab490
FT_GetDeviceInfo succeeded.  Device is type 5.
FT_EE_Read succeeded.

Signature1 = 0
Signature2 = -1
Version = 2
VendorId = 0x1313
ProductId = 0x8060
Manufacturer = Thorlabs
ManufacturerId = TL
Description = DC3100 - FLIM LED Driver
SerialNumber = TL0HWDCP
MaxPower = 0
PnP = 1
SelfPowered = 1
RemoteWakeup = 0
232R:
-----
	UseExtOsc = 0x0
	HighDriveIOs = 0x0
	EndpointSize = 0x40
	PullDownEnableR = 0x0
	SerNumEnableR = 0x1
	InvertTXD = 0x0
	InvertRXD = 0x0
	InvertRTS = 0x0
	InvertCTS = 0x0
	InvertDTR = 0x0
	InvertDSR = 0x0
	InvertDCD = 0x0
	InvertRI = 0x0
	Cbus0 = 0x2
	Cbus1 = 0x3
	Cbus2 = 0x1
	Cbus3 = 0x1
	Cbus4 = 0x5
	RIsD2XX = 0x0
Returning 0

Simple command send and read response:

bi@bipc101 ~/dc3100/release/examples/Simple $ sudo ./simple-dynamic 
Device 0 Serial Number - TL0HWDCP
Opened device TL0HWDCP
Calling FT_Write with this write-buffer:
 [0x53, 0x3F, 0x0A]
Calling FT_Read with this read-buffer:
 [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]
FT_Read read 11 bytes.  Read-buffer is now:
 [0x4D, 0x30, 0x30, 0x34, 0x31, 0x37, 0x31, 0x38, 0x36, 0x0D, 0x0A]
TL0HWDCP test passed.
Closed device TL0HWDCP

