/**
 * Asyn driver for the Thorlabs DC3100 - FLIM LED Driver.
 *
 * @author Hinko Kocevar
 * @date July 2016
 *
 */

#include <stdio.h>
#include <string.h>
#include <string>
#include <unistd.h>
#include <errno.h>
#include <time.h>

#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsEvent.h>
#include <epicsString.h>
#include <iocsh.h>
#include <epicsExport.h>
#include <epicsExit.h>

#include "tlDC3100.h"


// common status bits
#define STAT_NO_LED_CHANGED					0x0001
#define STAT_NO_LED							0x0002
#define STAT_VCC_FAIL_CHANGED				0x0004
#define STAT_VCC_FAIL						0x0008
#define STAT_OTP_CHANGED					0x0010
#define STAT_OTP							0x0020
#define STAT_LED_OPEN_CHANGED				0x0040
#define STAT_LED_OPEN						0x0080
#define STAT_LED_LIMIT_CHANGED				0x0100
#define STAT_LED_LIMIT						0x0200
#define STAT_OTP_HEAD_CHANGED				0x0400
#define STAT_OTP_HEAD						0x0800
#define STAT_IFC_REFRESH_CHANGED			0x1000

static const char *driverName = "TLDC3100";

//C Function prototypes to tie in with EPICS
static void tlDeviceTaskC(void *drvPvt);
static void exitHandler(void *drvPvt);

/** Constructor for Thorlabs driver; most parameters are simply passed to AsynPortDriver.
 * After calling the base class constructor this method creates a thread to collect the detector data,
 * and sets reasonable default values the parameters defined in this class and asynPortDriver.
 * \param[in] portName The name of the asyn port driver to be created.
 * \param[in] usbVID The USB VID (eg. 0x1313).
 * \param[in] usbPID The USB PID (eg. 0x8060).
 * \param[in] serialNumber The resource string that describes Thorlabs DC3100 instrument (eg. 37123456).
 * \param[in] priority The thread priority for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
 * \param[in] stackSize The stack size for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
 */
TLDC3100::TLDC3100(const char *portName, int usbVID, int usbPID,
		const char *serialNumber, int priority, int stackSize)

:
		asynPortDriver(portName, 1, NUM_TL_DET_PARAMS,
                asynInt32Mask | asynFloat64Mask | asynOctetMask | asynEnumMask | asynDrvUserMask, /* Interface mask */
                asynInt32Mask | asynFloat64Mask | asynOctetMask | asynEnumMask,  /* Interrupt mask */
                0, /* asynFlags.  This driver does not block and it is not multi-device, so flag is 0 */
                1, /* Autoconnect */
                0, /* Default priority */
                0) /* Default stack size*/
{
	int status = asynSuccess;
	static const char *functionName = "TLDC3100";
	char vidpid[TLDC3100_BUFFER_SIZE] = {0};

	mPosition = 0;
	mUsbVID = usbVID;
	if (mUsbVID == 0) {
		mUsbVID = 0x1313;
	}
	mUsbPID = usbPID;
	if (mUsbPID == 0) {
		mUsbPID = 0x8060;
	}
	strncpy(mWantSerialNumber, serialNumber, TLDC3100_BUFFER_SIZE);
	printf("DC3100 serial %s, VID 0x%04X, PID 0x%04X\n", mWantSerialNumber, mUsbVID, mUsbPID);

	memset(mSerialNumber, 0, TLDC3100_BUFFER_SIZE);
	memset(mDeviceName, 0, TLDC3100_BUFFER_SIZE);
	memset(mManufacturerName, 0, TLDC3100_BUFFER_SIZE);
	memset(mFirmwareVersion, 0, TLDC3100_BUFFER_SIZE);

	/* Create an EPICS exit handler */
	epicsAtExit(exitHandler, this);

	status = createParam(TlMessageString, asynParamOctet, &TlMessage);
	status |= createParam(TlMnfcString, asynParamOctet, &TlMnfc);
	status |= createParam(TlProdString, asynParamOctet, &TlProd);
	status |= createParam(TlSerialString, asynParamOctet, &TlSerial);
	status |= createParam(TlVIDPIDString, asynParamOctet, &TlVIDPID);
	status |= createParam(TlLimitCurrentString, asynParamFloat64, &TlLimitCurrent);
	status |= createParam(TlMaxLimitString, asynParamFloat64, &TlMaxLimit);
	status |= createParam(TlMaxFrequencyString, asynParamFloat64, &TlMaxFrequency);
	status |= createParam(TlOpModeString, asynParamInt32, &TlOpMode);
	status |= createParam(TlLEDControlString, asynParamInt32, &TlLEDControl);
	status |= createParam(TlModCurrentString, asynParamFloat64, &TlModCurrent);
	status |= createParam(TlModFrequencyString, asynParamFloat64, &TlModFrequency);
	status |= createParam(TlModDepthString, asynParamInt32, &TlModDepth);
	status |= createParam(TlConstCurrentString, asynParamFloat64, &TlConstCurrent);
	status |= createParam(TlDisplayBrightString, asynParamInt32, &TlDisplayBright);
	status |= createParam(TlStatusRegisterString, asynParamInt32, &TlStatusRegister);
	status |= createParam(TlErrorNoLEDString, asynParamInt32, &TlErrorNoLED);
	status |= createParam(TlErrorVccFailString, asynParamInt32, &TlErrorVccFail);
	status |= createParam(TlErrorChassisOTPString, asynParamInt32, &TlErrorChassisOTP);
	status |= createParam(TlErrorLEDOpenString, asynParamInt32, &TlErrorLEDOpen);
	status |= createParam(TlErrorOverLimitString, asynParamInt32, &TlErrorOverLimit);
	status |= createParam(TlErrorLEDOTPString, asynParamInt32, &TlErrorLEDOTP);
	status |= createParam(TlErrorRefreshString, asynParamInt32, &TlErrorRefresh);

	if (status) {
		printf("%s:%s: unable to create parameters\n", driverName, functionName);
		return;
	}

	// Use this to signal the device task to unblock.
	this->mDeviceEvent = epicsEventMustCreate(epicsEventEmpty);
	if (!this->mDeviceEvent) {
		printf("%s:%s epicsEventCreate failure for device event\n", driverName, functionName);
		return;
	}

	// addFTDevice() will try to find and initialize the device
	mInstr = NULL;

	/* Set some default values for parameters */

	status = setStringParam(TlMnfc, mManufacturerName);
	status |= setStringParam(TlProd, mDeviceName);
	// clear the serial value since device is not yet initialized
	status |= setStringParam(TlSerial, mDeviceName);
	status |= setStringParam(TlVIDPID, vidpid);

	callParamCallbacks();

	if (status) {
		printf("%s:%s: unable to set parameters\n", driverName, functionName);
		return;
	}

	if (stackSize == 0) {
		stackSize = epicsThreadGetStackSize(epicsThreadStackMedium);
	}

	/* for stopping thread */
	mFinish = 0;

	/* Create the thread that does USB device detection */
	status = (epicsThreadCreate("TLDC3100DeviceTask",
			epicsThreadPriorityMedium, stackSize, (EPICSTHREADFUNC) tlDeviceTaskC, this) == NULL);
	if (status) {
		printf("%s:%s: epicsThreadCreate failure for device task\n", driverName, functionName);
		return;
	}

	printf("DC3100 initialized OK!\n");
}

/**
 * Destructor.  Free resources and closes the ftd2xx library
 */
TLDC3100::~TLDC3100() {
	printf("Shutdown and freeing up memory...\n");
	this->lock();
	/* make sure thread is cleanly stopped */
	printf("Waiting for thread..\n");
	this->mFinish = 1;
	epicsEventSignal(mDeviceEvent);
	sleep(1);
	printf("Threads are down!\n");

	removeFTDevice();

	this->unlock();
}

/**
 * Add USB device.
 */
void TLDC3100::addFTDevice(void) {

	static const char *functionName = "addFTDevice";
	char *pcBufLD[6];
	char cBufLD[5][TLDC3100_BUFFER_SIZE];
	int	i;
	int	iNumDevs = 0;
	bool haveDevice = false;
	FT_DEVICE ftDevice;
    DWORD devid;
    char serialNumber[TLDC3100_BUFFER_SIZE] = {0};
    char deviceName[TLDC3100_BUFFER_SIZE] = {0};
    char vidpid[TLDC3100_BUFFER_SIZE] = {0};
    char firmwareVersion[TLDC3100_BUFFER_SIZE] = {0};

	try {
		printf("%s:%s: looking for DC3100 instruments\n", driverName,	functionName);
		for(i = 0; i < 5; i++) {
			pcBufLD[i] = cBufLD[i];
		}
		pcBufLD[5] = NULL;

		// use our VID and PID - THIS MANDATORY!!!!
		checkStatus("FT_SetVIDPID", FT_SetVIDPID(mUsbVID, mUsbPID));

		checkStatus("FT_ListDevices", FT_ListDevices(pcBufLD, &iNumDevs, FT_LIST_ALL | FT_OPEN_BY_SERIAL_NUMBER));

		for(i = 0; ( (i < 5) && (i < iNumDevs)); i++) {
			// XXX: For DC3100 serial number returned here is not the one on the
			//      label! We need to send 'S?\n' to get real serial number - see below.
			printf("Device %d Serial Number - %s\n", i, cBufLD[i]);

//			if (strncmp(mWantSerialNumber, cBufLD[i], 8) != 0) {
//				printf("Skipping the device, want serial number '%s'..\n", mSerialNumber);
//				continue;
//			}

			// This can fail if the ftdi_sio driver is loaded
			// use lsmod to check this and rmmod ftdi_sio to remove
			// also rmmod usbserial
			checkStatus("FT_OpenEx", FT_OpenEx(cBufLD[i], FT_OPEN_BY_SERIAL_NUMBER, &mInstr));
			printf("Opened DC3100 device with serial '%s'\n", cBufLD[i]);

			// get more info from the device
			checkStatus("FT_GetDeviceInfo", FT_GetDeviceInfo(mInstr, &ftDevice, &devid, serialNumber, deviceName, NULL));
            // deviceID contains encoded device ID
            // SerialNumber, Description contain 0-terminated strings
            printf("device VID 0x%04X, PID 0x%04X\n", (devid >> 16) & 0xFFFF, devid & 0xFFFF);
            printf("device serial '%s'\n", serialNumber);

//            if (strncmp(mWantSerialNumber, serialNumber, 8) != 0) {
//				printf("Serial number mismatch '%s' != '%s'\n", mSerialNumber, serialNumber);
//				printf("Skipping the device..\n");
//				checkStatus("FT_Close", FT_Close(mInstr));
//				continue;
//			}

            if (((devid >> 16) & 0xFFFF) != mUsbVID) {
				printf("USB VID mismatch '0x%04X' != '0x%04X'\n", mUsbVID, (devid >> 16) & 0xFFFF);
				printf("Skipping the device..\n");
				checkStatus("FT_Close", FT_Close(mInstr));
				continue;
			}

            if ((devid & 0xFFFF) != mUsbPID) {
				printf("USB PID mismatch '0x%04X' != '0x%04X'\n", mUsbPID, devid & 0xFFFF);
				printf("Skipping the device..\n");
				checkStatus("FT_Close", FT_Close(mInstr));
				continue;
			}

//            strncpy(mSerialNumber, serialNumber, strlen(serialNumber));
//            strncpy(mDeviceName, deviceName, strlen(deviceName));
            sprintf(mManufacturerName, "Thorlabs");
        	sprintf(vidpid, "0x%04X:0x%04X", mUsbVID, mUsbPID);

			// Set baud rate to 115200.
			checkStatus("FT_SetBaudRate", FT_SetBaudRate(mInstr, 115200));
			// 8 data bits, 1 stop bit, no parity
			checkStatus("FT_SetDataCharacteristics", FT_SetDataCharacteristics(mInstr, FT_BITS_8, FT_STOP_BITS_1, FT_PARITY_NONE));
			// Pre purge dwell 50ms.
			usleep(50000);
			// Purge the device.
			checkStatus("FT_Purge", FT_Purge(mInstr, FT_PURGE_RX | FT_PURGE_TX));
			// Post purge dwell 50ms.
			usleep(50000);
			// Reset device.
			checkStatus("FT_ResetDevice", FT_ResetDevice(mInstr));
			// Set flow control to RTS/CTS.
			checkStatus("FT_SetFlowControl", FT_SetFlowControl(mInstr, FT_FLOW_RTS_CTS, 0, 0));
			// Set RTS.
			checkStatus("FT_SetRts", FT_SetRts(mInstr));
			// Set Timeouts.
			checkStatus("FT_SetTimeouts", FT_SetTimeouts(mInstr, 3000, 3000));

			// NOTE: get the real serial number since the one in FT_ListDevices()
			getValue("S", serialNumber, sizeof(serialNumber));
			printf("DC3100 device serial number '%s'..\n", serialNumber);
            if (strncmp(mWantSerialNumber, serialNumber, 8) != 0) {
				printf("Serial number mismatch '%s' != '%s'\n", mWantSerialNumber, serialNumber);
				printf("Skipping the device..\n");
				checkStatus("FT_Close", FT_Close(mInstr));
				continue;
			}

            strncpy(mSerialNumber, serialNumber, strlen(serialNumber));
			getValue("N", deviceName, sizeof(deviceName));
            strncpy(mDeviceName, deviceName, strlen(deviceName));
			getValue("V", firmwareVersion, sizeof(firmwareVersion));
            strncpy(mFirmwareVersion, firmwareVersion, strlen(firmwareVersion));
            sprintf(mManufacturerName, "Thorlabs");
        	sprintf(vidpid, "0x%04X:0x%04X", mUsbVID, mUsbPID);

			haveDevice = true;
			// Device should be ready to receive commands and provide responses
		}

		if (! haveDevice) {
			asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s:%s: did not find device with serial '%s'\n", driverName,
					functionName, mWantSerialNumber);
			return;
		}
	} catch (const std::string &e) {
		asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s:%s: %s\n", driverName,
				functionName, e.c_str());
		return;
	}

	setStringParam(TlMnfc, mManufacturerName);
	setStringParam(TlProd, mDeviceName);
	setStringParam(TlSerial, mSerialNumber);
	setStringParam(TlVIDPID, vidpid);
	setStringParam(TlMessage, "USB device is attached!");
	callParamCallbacks();

	asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER,
			"%s:%s: filter flipper '%s' successfully initialized\n",
			driverName, functionName, mSerialNumber);
}

/**
 * Remove USB device.
 */
void TLDC3100::removeFTDevice(void) {
	static const char *functionName = "removeFTDevice";

	try {
		/* Close instrument handle if opened */
		printf("DC3100 shutting down as part of IOC exit.\n");
		if (mInstr != NULL) {
			checkStatus("FT_Close", FT_Close(mInstr));
			mInstr = NULL;
		}
	} catch (const std::string &e) {
		asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s:%s: %s\n", driverName,
				functionName, e.c_str());
		return;
	}

	setStringParam(TlMessage, "USB device is detached!");
	callParamCallbacks();

	asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER,
			"%s:%s: DC3100 successfully shutdown\n", driverName, functionName);
}

/**
 * Exit handler, delete the DC3100 object.
 */
static void exitHandler(void *drvPvt) {
	TLDC3100 *pDC3100 = (TLDC3100 *) drvPvt;
	delete pDC3100;
}

/** Report status of the driver.
 * Prints details about the detector in us if details>0.
 * It then calls the asynPortDriver::report() method.
 * \param[in] fp File pointed passed by caller where the output is written to.
 * \param[in] details Controls the level of detail in the report. */
void TLDC3100::report(FILE *fp, int details) {
	static const char *functionName = "report";

	fprintf(fp, "Thorlabs DC3100 port=%s\n", this->portName);
	if (details > 0) {
		try {
			fprintf(fp, "  Manufacturer: %s\n", mManufacturerName);
			fprintf(fp, "  Model: %s\n", mDeviceName);
			fprintf(fp, "  Serial number: %s\n", mSerialNumber);
			fprintf(fp, "  Firmware version: %s\n", mFirmwareVersion);
			fprintf(fp, "  VID: 0x%04X\n", mUsbVID);
			fprintf(fp, "  PID: 0x%04X\n", mUsbPID);

		} catch (const std::string &e) {
			asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s:%s: %s\n",
					driverName, functionName, e.c_str());
		}
	}
	// Call the base class method
	asynPortDriver::report(fp, details);
}

asynStatus TLDC3100::sendData(char *wrBuf, unsigned int wrLen) {
	unsigned int wrReal;
	static const char *functionName = "sendData";

	printf("SET BUF [%d]: %s", wrLen, wrBuf);

	try {
		checkStatus("FT_Write", FT_Write(mInstr, wrBuf, wrLen, &wrReal));

		if (wrLen != wrReal) {
			asynPrint(pasynUserSelf, ASYN_TRACE_ERROR,
					"%s:%s: Failed to send all data!\n", driverName, functionName);
			return asynError;
		}
	} catch (const std::string &e) {
		asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s:%s: %s\n", driverName,
				functionName, e.c_str());
		return asynError;
	}

	return asynSuccess;
}

asynStatus TLDC3100::recvData(char *wrBuf, unsigned int wrLen, char *rdBuf, unsigned int rdLen) {
	unsigned int wrReal, rdPart, rdReal, rdTotal, tries;
	char *pBuf;
	static const char *functionName = "recvData";

	printf("SET BUF [%d]: %s", wrLen, wrBuf);

	tries = 10;
	try {
		// Flush the tx / rx buffers
		checkStatus("FT_Purge", FT_Purge(mInstr, FT_PURGE_RX | FT_PURGE_TX));
		usleep(5000);

		checkStatus("FT_Write", FT_Write(mInstr, wrBuf, wrLen, &wrReal));
		if (wrLen != wrReal) {
			asynPrint(pasynUserSelf, ASYN_TRACE_ERROR,
					"%s:%s: Failed to send all data!\n", driverName, functionName);
			return asynError;
		}

		rdTotal = 0;
		pBuf = rdBuf;
		while (rdTotal < rdLen) {
			checkStatus("FT_GetQueueStatus", FT_GetQueueStatus(mInstr, &rdPart));
			printf("FT_GetQueueStatus() partial return %d\n", rdPart);
			if (rdPart == 0) {
				usleep(5000);
				continue;
			}
			// read only what is waiting for us
			checkStatus("FT_Read", FT_Read(mInstr, pBuf, rdPart, &rdReal));
			pBuf += rdReal;
			rdTotal += rdReal;
			printf("rdTotal %d\n", rdTotal);

			// if last byte is '\n' then we have complete response
//			if ((rdTotal > 0) && (rdBuf[rdTotal-1] == 0x0A)) {
			if (rdBuf[rdTotal-1] == 0x0A) {
				rdBuf[rdTotal] = 0;
				break;
			}

			usleep(5000);
			if (tries-- == 0) {
				asynPrint(pasynUserSelf, ASYN_TRACE_ERROR,
						"%s:%s: Failed to receive all data!\n", driverName, functionName);
				return asynError;
			}
		}
	} catch (const std::string &e) {
		asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s:%s: %s\n", driverName,
				functionName, e.c_str());
		return asynError;
	}

	dumpBuffer(rdBuf, rdTotal);
	printf("GET BUF [%d]: %s", rdTotal, rdBuf);

	return asynSuccess;
}

/*---------------------------------------------------------------------------
 Parses the error string.
---------------------------------------------------------------------------*/
bool TLDC3100::parseError(char *orgMsg, int *errCode, char errMsg[]) {

	if (orgMsg == NULL) {
		return false;
	}

	if (sscanf(orgMsg, "ERROR %d:%[^\r\n]", errCode, errMsg) < 2)	{
		return false;
	}

	return true;
}

/*---------------------------------------------------------------------------
 Gets the error code from the last command and adds it to list if necessary.
---------------------------------------------------------------------------*/
asynStatus TLDC3100::getLastError(int *errCode) {
	char buf[TLDC3100_BUFFER_SIZE];
	char errMsg[TLDC3100_BUFFER_SIZE] = {0};
	int len;
//	DWORD dwBytesRead, dwBytesWritten;
	asynStatus status;
//	static const char *functionName = "getLastError";

	if (mInstr == NULL) {
		return asynDisconnected;
	}

	// prepare the setting command
	len = sprintf(buf, "E?\n");

//	try {
//		// Purge the device.
//		checkStatus("FT_Purge", FT_Purge(mInstr, FT_PURGE_RX | FT_PURGE_TX));
//		// Post purge dwell 50ms.
////		usleep(50000);
//		// transmit the getting command
//		checkStatus("FT_Write", FT_Write(mInstr, buf, len, &dwBytesWritten));
//		// receive the value
//		checkStatus("FT_Read", FT_Read(mInstr, buf, sizeof(buf), &dwBytesRead));
//		printf("GET ERR: %s", buf);
//	} catch (const std::string &e) {
//		asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s:%s: %s\n", driverName,
//				functionName, e.c_str());
//		return asynError;
//	}
//
//	// parse for error
//	if(! parseError(buf, errCode, errMsg)) {
//		return asynError;
//	}

	status = recvData(buf, len, buf, sizeof(buf));
	if (status) {
		return status;
	}

	// parse for error
	if(! parseError(buf, errCode, errMsg)) {
		return asynError;
	}

	if (*errCode == 0) {
		return asynSuccess;
	}

	// update message string with error message
	setStringParam(TlMessage, errMsg);
	callParamCallbacks();

	// Get private data
//	if((err = viGetAttribute(instr, VI_ATTR_USER_DATA, &data))) return (err);
	// calculate our error code
//	*errCode += VI_INSTR_ERROR_OFFSET;
	// Add instrument error to dynamic error list
//	data->errList = DC3100_dynErrlist_add(data->errList, *errCode, errMsg);

	return asynError;
}

asynStatus TLDC3100::getValue(const char *cmd, char *rdBuf, unsigned int rdLen) {
	char buf[TLDC3100_BUFFER_SIZE];
	int len;
	int errCode;
	asynStatus status;

	if (mInstr == NULL) {
		return asynDisconnected;
	}

	// prepare the setting command
	len = sprintf(buf, "%s?\n", cmd);

	status = recvData(buf, len, buf, sizeof(buf));
	if (status) {
		return status;
	}

	// get the value from response
	errCode = sscanf(buf, " %s \n", rdBuf);
	if (errCode < 1) {
		return asynError;
	}

	// ask for error
	status = getLastError(&errCode);

	// return error code
	return status;
}

asynStatus TLDC3100::setValue(const char *cmd, epicsInt32 value) {
	char buf[TLDC3100_BUFFER_SIZE];
	int len;
//	DWORD dwBytesWritten;
	asynStatus status;
	int errCode;
//	static const char *functionName = "setValue";

	if (mInstr == NULL) {
		return asynDisconnected;
	}

	// prepare the setting command
	len = sprintf(buf, "%s %d\n", cmd, value);
//	printf("SET BUF: %s", buf);

//	try {
//		// transmit the setting command
//		checkStatus("FT_Write", FT_Write(mInstr, buf, len, &dwBytesWritten));
//	} catch (const std::string &e) {
//		asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s:%s: %s\n", driverName,
//				functionName, e.c_str());
//		return asynError;
//	}

	status = sendData(buf, len);
	if (status) {
		return status;
	}

	sleep(1);

	// ask for error
	status = getLastError(&errCode);

	// return error code
	return status;
}

asynStatus TLDC3100::getValue(const char *cmd, epicsInt32 *value) {
	char buf[TLDC3100_BUFFER_SIZE];
//	char *pBuf;
	int len;
//	unsigned int rdSz, rdTotal;
//	int tries = 10;
//	DWORD dwBytesRead, dwBytesWritten;
	asynStatus status;
	int errCode;
//	static const char *functionName = "getValue";

	if (mInstr == NULL) {
		return asynDisconnected;
	}

	// prepare the setting command
	len = sprintf(buf, "%s?\n", cmd);
//	printf("GET CMD: %s", buf);
//
//	try {
//		// Purge the device.
//		checkStatus("FT_Purge", FT_Purge(mInstr, FT_PURGE_RX | FT_PURGE_TX));
//		// Post purge dwell 50ms.
////		usleep(50000);
//		// transmit the getting command
//		checkStatus("FT_Write", FT_Write(mInstr, buf, len, &dwBytesWritten));
//		// receive the value
//		rdTotal = 0;
//		pBuf = buf;
//		while (rdTotal < sizeof(buf)) {
//			checkStatus("FT_GetQueueStatus", FT_GetQueueStatus(mInstr, &rdSz));
//			printf("FT_GetQueueStatus() partial return %d\n", rdSz);
//			if (rdSz == 0) {
//				continue;
//			}
//			checkStatus("FT_Read", FT_Read(mInstr, pBuf, rdSz, &dwBytesRead));
//			pBuf += dwBytesRead;
//			rdTotal += dwBytesRead;
//			printf("rdTotal %d\n", rdTotal);
//
//			if ((rdTotal > 0) && (buf[rdTotal-1] == 0x0A)) {
//				break;
//			}
//
//			usleep(50000);
//			if (tries-- == 0) {
//				printf("FT_GetQueueStatus() failed!\n");
//				status = asynError;
//				return status;
//			}
//		}
//		dumpBuffer(buf, rdTotal);
//		printf("GET BUF: %s", buf);
//		ret = sscanf(buf, " %d \n", value);
//		if (ret < 1) {
//			return asynError;
//		}
//	} catch (const std::string &e) {
//		asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s:%s: %s\n", driverName,
//				functionName, e.c_str());
//		return asynError;
//	}

	status = recvData(buf, len, buf, sizeof(buf));
	if (status) {
		return status;
	}

	// get the value from response
	errCode = sscanf(buf, " %d \n", value);
	if (errCode < 1) {
		return asynError;
	}

	// ask for error
	status = getLastError(&errCode);

	// return error code
	return status;
}

asynStatus TLDC3100::setValue(const char *cmd, epicsFloat64 value) {
	char buf[TLDC3100_BUFFER_SIZE];
	int len;
//	DWORD dwBytesWritten;
	asynStatus status;
	int errCode;
//	static const char *functionName = "setValue";

	if (mInstr == NULL) {
		return asynDisconnected;
	}

	// prepare the setting command
	len = sprintf(buf, "%s %.8e\n", cmd, value);
//	printf("SET BUF: %s", buf);
//
//	try {
//		// transmit the setting command
//		checkStatus("FT_Write", FT_Write(mInstr, buf, len, &dwBytesWritten));
//	} catch (const std::string &e) {
//		asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s:%s: %s\n", driverName,
//				functionName, e.c_str());
//		return asynError;
//	}

	status = sendData(buf, len);
	if (status) {
		return status;
	}

	sleep(1);

	// ask for error
	status = getLastError(&errCode);

	// return error code
	return status;
}

asynStatus TLDC3100::getValue(const char *cmd, epicsFloat64 *value) {
	char buf[TLDC3100_BUFFER_SIZE];
//	char *pBuf;
	int len;
//	unsigned int rdSz, rdTotal;
//	int tries = 10;
//	DWORD dwBytesRead, dwBytesWritten;
	asynStatus status;
	int errCode;
//	static const char *functionName = "getValue";

	if (mInstr == NULL) {
		return asynDisconnected;
	}

	// prepare the setting command
	len = sprintf(buf, "%s?\n", cmd);
//	printf("GET CMD: %s", buf);
//
//	try {
//		// Purge the device.
//		checkStatus("FT_Purge", FT_Purge(mInstr, FT_PURGE_RX | FT_PURGE_TX));
//		// Post purge dwell 50ms.
////		usleep(50000);
//		// transmit the getting command
//		checkStatus("FT_Write", FT_Write(mInstr, buf, len, &dwBytesWritten));
//		// receive the value
//		rdTotal = 0;
//		pBuf = buf;
//		while (rdTotal < sizeof(buf)) {
//			checkStatus("FT_GetQueueStatus", FT_GetQueueStatus(mInstr, &rdSz));
//			printf("FT_GetQueueStatus() partial return %d\n", rdSz);
//			if (rdSz == 0) {
//				continue;
//			}
//			checkStatus("FT_Read", FT_Read(mInstr, pBuf, rdSz, &dwBytesRead));
//			pBuf += dwBytesRead;
//			rdTotal += dwBytesRead;
//			printf("rdTotal %d\n", rdTotal);
//
//			if ((rdTotal > 0) && (buf[rdTotal-1] == 0x0A)) {
//				break;
//			}
//
//			usleep(50000);
//			if (tries-- == 0) {
//				printf("FT_GetQueueStatus() failed!\n");
//				status = asynError;
//				return status;
//			}
//		}
//		dumpBuffer(buf, rdTotal);
//		printf("GET BUF: %s", buf);
//		ret = sscanf(buf, " %le \n", value);
//		if (ret < 1) {
//			return asynError;
//		}
//	} catch (const std::string &e) {
//		asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s:%s: %s\n", driverName,
//				functionName, e.c_str());
//		return asynError;
//	}

	status = recvData(buf, len, buf, sizeof(buf));
	if (status) {
		return status;
	}

	// get the value from response
	errCode = sscanf(buf, " %le \n", value);
	if (errCode < 1) {
		return asynError;
	}

	// ask for error
	status = getLastError(&errCode);

	// return error code
	return status;
}

asynStatus TLDC3100::handleError(int function, epicsInt32 value, epicsInt32 changeBit, epicsInt32 valueBit) {

	setIntegerParam(function, 0);
	if (value & changeBit) {
		if (value & valueBit) {
			setIntegerParam(function, 1);
//		} else {
//			setIntegerParam(function, 0);
		}
	}

	return asynSuccess;
}

/** Called when asyn clients call pasynInt32->write().
 * This function performs actions for some parameters, including TlFLip, etc.
 * For all parameters it sets the value in the parameter library and calls any registered callbacks..
 * \param[in] pasynUser pasynUser structure that encodes the reason and address.
 * \param[in] value Value to write. */
asynStatus TLDC3100::writeInt32(asynUser *pasynUser, epicsInt32 value) {
	int function = pasynUser->reason;

	asynStatus status = asynSuccess;
	static const char *functionName = "writeInt32";

	//Set in param lib so the user sees a readback straight away. Save a backup in case of errors.
	status = setIntegerParam(function, value);
	printf("%s func %d value %d\n", functionName, function, value);

	if (function == TlLEDControl) {
		status = setValue("O", value);
	} else if (function == TlOpMode) {
		status = setValue("M", value);
	} else if (function == TlModDepth) {
		status = setValue("D", value);
	} else if (function == TlDisplayBright) {
		status = setValue("B", value);
	} else {
		status = asynPortDriver::writeInt32(pasynUser, value);
	}

	if (status == asynSuccess) {
		//For a successful write, clear the error message.
		setStringParam(TlMessage, " ");
	}

	/* Do callbacks so higher layers see any changes */
	callParamCallbacks();

	if (status)
		asynPrint(pasynUser, ASYN_TRACE_ERROR,
				"%s:%s: error, status=%d function=%d, value=%d\n", driverName,
				functionName, status, function, value);
	else
		asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
				"%s:%s: function=%d, value=%d\n", driverName, functionName,
				function, value);
	return status;
}

/** Called when asyn clients call pasynInt32->read().
  * The base class implementation simply returns the value from the parameter library.
  * Derived classes rarely need to reimplement this function.
  * \param[in] pasynUser pasynUser structure that encodes the reason and address.
  * \param[out] value Address of the value to read. */
asynStatus TLDC3100::readInt32(asynUser *pasynUser, epicsInt32 *value) {
    int function = pasynUser->reason;
    int addr=0;
    asynStatus status = asynSuccess;
    epicsTimeStamp timeStamp; getTimeStamp(&timeStamp);
    static const char *functionName = "readInt32";

    status = getAddress(pasynUser, &addr);
    if (status != asynSuccess) {
    	return status;
    }

    /* We just read the current value of the parameter from the parameter library.
     * Those values are updated whenever anything could cause them to change */
//    status = (asynStatus) getIntegerParam(addr, function, value);
    /* Set the timestamp */
    pasynUser->timestamp = timeStamp;

	if (function == TlLEDControl) {
		status = getValue("O", value);
		status = setIntegerParam(function, *value);
		printf("%s func %d value %d\n", functionName, function, *value);
	} else if (function == TlOpMode) {
		status = getValue("M", value);
		status = setIntegerParam(function, *value);
		printf("%s func %d value %d\n", functionName, function, *value);
	} else if (function == TlModDepth) {
		status = getValue("D", value);
		status = setIntegerParam(function, *value);
		printf("%s func %d value %d\n", functionName, function, *value);
	} else if (function == TlDisplayBright) {
		status = getValue("B", value);
		status = setIntegerParam(function, *value);
		printf("%s func %d value %d\n", functionName, function, *value);
	} else if (function == TlStatusRegister) {
		status = getValue("R", value);
		status = setIntegerParam(function, *value);
		printf("%s func %d value %d\n", functionName, function, *value);
		handleError(TlErrorNoLED, *value, STAT_NO_LED_CHANGED, STAT_NO_LED);
		handleError(TlErrorVccFail, *value, STAT_VCC_FAIL_CHANGED, STAT_VCC_FAIL);
		handleError(TlErrorChassisOTP, *value, STAT_OTP_CHANGED, STAT_OTP);
		handleError(TlErrorLEDOpen, *value, STAT_LED_OPEN_CHANGED, STAT_LED_OPEN);
		handleError(TlErrorOverLimit, *value, STAT_LED_LIMIT_CHANGED, STAT_LED_LIMIT);
		handleError(TlErrorLEDOTP, *value, STAT_OTP_HEAD_CHANGED, STAT_OTP_HEAD);
		handleError(TlErrorRefresh, *value, STAT_IFC_REFRESH_CHANGED, STAT_IFC_REFRESH_CHANGED);
		/* Do callbacks so higher layers see any changes */
		callParamCallbacks();
	} else {
		status = asynPortDriver::readInt32(pasynUser, value);
	}

    if (status) {
        epicsSnprintf(pasynUser->errorMessage, pasynUser->errorMessageSize,
                  "%s:%s: status=%d, function=%d, value=%d",
                  driverName, functionName, status, function, *value);
    } else {
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
              "%s:%s: function=%d, value=%d\n",
              driverName, functionName, function, *value);
    }

    return status;
}

/** Called when asyn clients call pasynFloat64->write().
 * This function performs actions for some parameters, including TlLimitCurrent, etc.
 * For all parameters it sets the value in the parameter library and calls any registered callbacks..
 * \param[in] pasynUser pasynUser structure that encodes the reason and address.
 * \param[in] value Value to write. */
asynStatus TLDC3100::writeFloat64(asynUser *pasynUser, epicsFloat64 value) {
	int function = pasynUser->reason;

	asynStatus status = asynSuccess;
	static const char *functionName = "writeFloat64";

	//Set in param lib so the user sees a readback straight away. Save a backup in case of errors.
	status = setDoubleParam(function, value);
	printf("%s func %d value %f\n", functionName, function, value);

	if (function == TlLimitCurrent) {
		status = setValue("L", value);
	} else if (function == TlMaxLimit) {
		status = setValue("ML", value);
	} else if (function == TlMaxFrequency) {
		status = setValue("MF", value);
	} else if (function == TlModCurrent) {
		status = setValue("CM", value);
	} else if (function == TlModFrequency) {
		status = setValue("F", value);
	} else if (function == TlModDepth) {
		status = setValue("D", value);
	} else if (function == TlConstCurrent) {
		status = setValue("CC", value);
	} else {
		status = asynPortDriver::writeFloat64(pasynUser, value);
	}

	if (status == asynSuccess) {
		//For a successful write, clear the error message.
		setStringParam(TlMessage, " ");
	}

	/* Do callbacks so higher layers see any changes */
	callParamCallbacks();

	if (status) {
		asynPrint(pasynUser, ASYN_TRACE_ERROR,
				"%s:%s: error, status=%d function=%d, value=%f\n", driverName,
				functionName, status, function, value);
	} else {
		asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
				"%s:%s: function=%d, value=%f\n", driverName, functionName,
				function, value);
	}

	return status;
}

/** Called when asyn clients call pasynFloat64->read().
  * The base class implementation simply returns the value from the parameter library.
  * Derived classes rarely need to reimplement this function.
  * \param[in] pasynUser pasynUser structure that encodes the reason and address.
  * \param[in] value Address of the value to read. */
asynStatus TLDC3100::readFloat64(asynUser *pasynUser, epicsFloat64 *value)
{
    int function = pasynUser->reason;
    int addr=0;
    asynStatus status = asynSuccess;
    epicsTimeStamp timeStamp; getTimeStamp(&timeStamp);
    static const char *functionName = "readFloat64";

    status = getAddress(pasynUser, &addr);
    if (status != asynSuccess) {
    	return status;
    }

    /* We just read the current value of the parameter from the parameter library.
     * Those values are updated whenever anything could cause them to change */
//    status = (asynStatus) getDoubleParam(addr, function, value);
    /* Set the timestamp */
    pasynUser->timestamp = timeStamp;

	if (function == TlLimitCurrent) {
		status = getValue("L", value);
		status = setDoubleParam(function, *value);
		printf("%s func %d value %f\n", functionName, function, *value);
	} else if (function == TlMaxLimit) {
		status = getValue("ML", value);
		status = setDoubleParam(function, *value);
		printf("%s func %d value %f\n", functionName, function, *value);
	} else if (function == TlMaxFrequency) {
		status = getValue("MF", value);
		status = setDoubleParam(function, *value);
		printf("%s func %d value %f\n", functionName, function, *value);
	} else if (function == TlModCurrent) {
		status = getValue("CM", value);
		status = setDoubleParam(function, *value);
		printf("%s func %d value %f\n", functionName, function, *value);
	} else if (function == TlModFrequency) {
		status = getValue("F", value);
		status = setDoubleParam(function, *value);
		printf("%s func %d value %f\n", functionName, function, *value);
	} else if (function == TlModDepth) {
		status = getValue("D", value);
		status = setDoubleParam(function, *value);
		printf("%s func %d value %f\n", functionName, function, *value);
	} else if (function == TlConstCurrent) {
		status = getValue("CC", value);
		status = setDoubleParam(function, *value);
		printf("%s func %d value %f\n", functionName, function, *value);
	} else {
		status = asynPortDriver::readFloat64(pasynUser, value);
	}

    if (status) {
        epicsSnprintf(pasynUser->errorMessage, pasynUser->errorMessageSize,
                  "%s:%s: status=%d, function=%d, value=%f",
                  driverName, functionName, status, function, *value);
    } else {
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
              "%s:%s: function=%d, value=%f\n",
              driverName, functionName, function, *value);
    }

    return status;
}

/**
 * Function to check the return status of Thorlabs SDK library functions.
 * @param returnStatus The return status of the SDK function
 * @return 0=success. Does not return in case of failure.
 * @throw std::string An exception is thrown in case of failure.
 */
unsigned int TLDC3100::checkStatus(const char *fnc, FT_STATUS returnStatus) {
	char ebuf[256];

	if (returnStatus == FT_OK) {
		return 0;
	} else {
		// Print error
		memset(ebuf, 0, sizeof(ebuf));
		sprintf(ebuf, "%s() failed with %d", fnc, (int)returnStatus);
		// Set the error for user to see
		setStringParam(TlMessage, ebuf);

		// Close the device if it was removed (USB cable unplugged)
		if ((returnStatus == FT_IO_ERROR) || (returnStatus == FT_INSUFFICIENT_RESOURCES)) {
			removeFTDevice();
		}

		throw std::string(ebuf);
	}

	return 0;
}

void TLDC3100::deviceTask(void) {
	static const char *functionName = "deviceTask";

	printf("Entered the task..\n");
	epicsEventWaitWithTimeout(mDeviceEvent, 1.0);
	printf("Running the task..\n");

	while (1) {

		if (this->mFinish) {
			asynPrint(pasynUserSelf, ASYN_TRACE_FLOW,
					"%s:%s: Stopping thread!\n", driverName, functionName);
			break;
		}

		this->lock();

		// Try to add USB device if not yet present
		if (mInstr == NULL) {
			printf("Adding the device..\n");
			addFTDevice();
		}

		this->unlock();

		epicsEventWaitWithTimeout(mDeviceEvent, 2.0);
	}

	printf("Device thread is down!\n");
}

void TLDC3100::dumpBuffer(char *buffer, unsigned int elements) {
	unsigned int j;
	printf(" [");
	for (j = 0; j < elements; j++) {
		if (j > 0)
			printf(", ");
		printf("0x%02X", (unsigned int)buffer[j]);
	}
	printf("]\n");
}
//
//
//asynStatus TLDC3100::doBlinkLED() {
//	asynStatus status = asynSuccess;
//
//	// blink the LED on the flipper
//	unsigned int wrSz;
//    unsigned char wrBuf[6];
//    unsigned int bufLen = 6;
//
//    if (! mInstr) {
//    	return asynDisconnected;
//    }
//
//    /* issue a MGMSG_MOD_IDENTITY command to the DC3100 */
//    wrBuf[0] = 0x23;
//    wrBuf[1] = 0x02;
//    wrBuf[2] = 0x00;
//    wrBuf[3] = 0x00;
//    wrBuf[4] = 0x50;
//    wrBuf[5] = 0x01;
//
//	/* Write */
//    dumpBuffer(wrBuf, bufLen);
//	checkStatus("FT_Write", FT_Write(mInstr, wrBuf, bufLen, &wrSz));
//	if (wrSz != bufLen) {
//		printf("FT_Write only wrote %d (of %d) bytes\n", (int)wrSz, bufLen);
//		status = asynError;
//	}
//
//	return status;
//}
//
//asynStatus TLDC3100::getPosition() {
//	asynStatus status = asynSuccess;
//
//	// flip the position on the flipper
//	unsigned int wrSz = 0;
//	unsigned int rdSz = 0;
//	unsigned int rdSzIn = 0;
//    unsigned char wrBuf[6];
//    unsigned char rdBuf[12];
//    unsigned int bufLen = 6;
//    unsigned int tries = 10;
//
//    if (! mInstr) {
//    	return asynDisconnected;
//    }
//
//    /* issue a MGMSG_MOT_REQ_STATUSBITS command to the DC3100 */
//    wrBuf[0] = 0x29;
//    wrBuf[1] = 0x04;
//    wrBuf[2] = 0x01;
//    wrBuf[3] = 0x00;
//    wrBuf[4] = 0x50;
//    wrBuf[5] = 0x01;
//
//	/* Write */
//    dumpBuffer(wrBuf, bufLen);
//	checkStatus("FT_Write", FT_Write(mInstr, wrBuf, bufLen, &wrSz));
//	if (wrSz != bufLen) {
//		printf("FT_Write only wrote %d (of %d) bytes\n", (int)wrSz, bufLen);
//		status = asynError;
//	}
//	bufLen = 12;
//	memset(rdBuf, 0xFF, bufLen);
//    while (rdSz < bufLen) {
//		checkStatus("FT_GetQueueStatus", FT_GetQueueStatus(mInstr, &rdSz));
//		printf("FT_GetQueueStatus() partial return %d\n", rdSz);
//		usleep(50000);
//		if (tries-- == 0) {
//			printf("FT_GetQueueStatus() failed to return %d\n", bufLen);
//    		status = asynError;
//    	    return status;
//		}
//    }
//	printf("FT_GetQueueStatus() final return %d\n", rdSz);
//
//	checkStatus("FT_Read", FT_Read(mInstr, rdBuf, rdSz, &rdSzIn));
//    dumpBuffer(rdBuf, rdSzIn);
//    if (rdSzIn != rdSz) {
//            printf("FT_Read only read %d (of %d) bytes\n", (int)rdSzIn, (int)rdSz);
//    		status = asynError;
//    }
//
//    // 1 == forward limit switch is enabled (vertical position)
//    // 2 == reverse limit switch is enabled (horizontal position)
//    // the flip value needs to be just opposite of the position value!
//    mPosition = rdBuf[8];
//
//    return status;
//}
//
//asynStatus TLDC3100::doFlip() {
//	asynStatus status = asynSuccess;
//
//	// flip the position on the flipper
//	unsigned int wrSz = 0;
//	unsigned int rdSz = 0;
//	unsigned int rdSzIn = 0;
//    unsigned char wrBuf[6];
//    unsigned char rdBuf[20];
//    unsigned int bufLen = 6;
//
//    if (! mInstr) {
//    	return asynDisconnected;
//    }
//
//    // no need to issue MGMSG_MOD_REQ_CHANENABLESTATE
//
//#if 0
//    /* issue a MGMSG_MOD_REQ_CHANENABLESTATE command to the DC3100 */
//    wrBuf[0] = 0x11;
//    wrBuf[1] = 0x02;
//    wrBuf[2] = 0x01;
//    wrBuf[3] = 0x00;
//    wrBuf[4] = 0x50;
//    wrBuf[5] = 0x01;
//
//	/* Write */
//    dumpBuffer(wrBuf, bufLen);
//	checkStatus("FT_Write", FT_Write(mInstr, wrBuf, bufLen, &wrSz));
//	if (wrSz != bufLen) {
//		printf("FT_Write only wrote %d (of %d) bytes\n", (int)wrSz, bufLen);
//		status = asynError;
//	}
//	memset(rdBuf, 0xFF, bufLen);
//    while (rdSz < bufLen) {
//		checkStatus("FT_GetQueueStatus", FT_GetQueueStatus(mInstr, &rdSz));
//    }
//	printf("FT_GetQueueStatus() returned %d\n", rdSz);
//	checkStatus("FT_Read", FT_Read(mInstr, rdBuf, rdSz, &rdSzIn));
//    dumpBuffer(rdBuf, rdSzIn);
//    if (rdSzIn != rdSz) {
//            printf("FT_Read only read %d (of %d) bytes\n", (int)rdSzIn, (int)rdSz);
//    		status = asynError;
//    }
//#endif
//
//    /* issue a MGMSG_MOT_MOVE_JOG command to the DC3100 */
//    char position = 0;
//    if (mPosition == DC3100_VERTICAL) {
//    	position = DC3100_HORIZONTAL;
//    } else if (mPosition == DC3100_HORIZONTAL) {
//    	position = DC3100_VERTICAL;
//    }
//    wrBuf[0] = 0x6A;
//    wrBuf[1] = 0x04;
//    wrBuf[2] = 0x01;
//    wrBuf[3] = position;
//    wrBuf[4] = 0x50;
//    wrBuf[5] = 0x01;
//
//    printf("going to position %d\n", position);
//
//	/* Write */
//    dumpBuffer(wrBuf, bufLen);
//	checkStatus("FT_Write", FT_Write(mInstr, wrBuf, bufLen, &wrSz));
//	if (wrSz != bufLen) {
//		printf("FT_Write only wrote %d (of %d) bytes\n", (int)wrSz, bufLen);
//		status = asynError;
//	}
//
//    printf("current position %d\n", position);
//    status = setIntegerParam(TlFlip, position);
//
//	return status;
//}

static void tlDeviceTaskC(void *drvPvt) {
	TLDC3100 *pPvt = (TLDC3100 *) drvPvt;

	pPvt->deviceTask();
}

/** IOC shell configuration command for Thorlabs driver
 * \param[in] portName The name of the asyn port driver to be created.
 * \param[in] usbVID The USB VID (eg. 0x0403).
 * \param[in] usbPID The USB PID (eg. 0xFAF0).
 * \param[in] serialNumber The resource string that describes Thorlabs DC3100 instrument (eg. 37123456).
 * \param[in] priority The thread priority for the asyn port driver thread
 * \param[in] stackSize The stack size for the asyn port driver thread
 */
extern "C" {
int tlDC3100Config(const char *portName, int usbVID, int usbPID,
		const char *serialNumber, int priority, int stackSize) {
	/*Instantiate class.*/
	new TLDC3100(portName, usbVID, usbPID, serialNumber, priority, stackSize);
	return (asynSuccess);
}

/* Code for iocsh registration */

/* tlDC3100Config */
static const iocshArg tlDC3100ConfigArg0 = { "Port name", iocshArgString };
static const iocshArg tlDC3100ConfigArg1 = { "USB VID", iocshArgInt };
static const iocshArg tlDC3100ConfigArg2 = { "USB PID", iocshArgInt };
static const iocshArg tlDC3100ConfigArg3 = { "Serial Number", iocshArgString };
static const iocshArg tlDC3100ConfigArg4 = { "priority", iocshArgInt };
static const iocshArg tlDC3100ConfigArg5 = { "stackSize", iocshArgInt };
static const iocshArg * const tlDC3100ConfigArgs[] = {
		&tlDC3100ConfigArg0,
		&tlDC3100ConfigArg1,
		&tlDC3100ConfigArg2,
		&tlDC3100ConfigArg3,
		&tlDC3100ConfigArg4,
		&tlDC3100ConfigArg5
};

static const iocshFuncDef configTLDC3100 = { "tlDC3100Config", 6, tlDC3100ConfigArgs };
static void configTLDC3100CallFunc(const iocshArgBuf *args) {
	tlDC3100Config(args[0].sval,
			args[1].ival,
			args[2].ival,
			args[3].sval,
			args[4].ival,
			args[5].ival);
}

static void tlDC3100Register(void) {

	iocshRegister(&configTLDC3100, configTLDC3100CallFunc);
}

epicsExportRegistrar(tlDC3100Register);

} /* extern "C" */
