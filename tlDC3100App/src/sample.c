//==============================================================================
//
// Title:      sample.c
// Purpose:    Simple pure C program to show how to communicate with
//					Thorlabs DC3100 - FLIM LED Driver.
//
//					This program will try to find a DC3100, open it and configure
//					the constant current mode to specified values. 
//					In case more than one DC3100 is connected to the PC, the first
//					one will be chosen.
//
//
// Created on: Jul-08-2009
//	Author:		Olaf Wohlmann (owohlmann@thorlabs.com)
// Copyright:  Thorlabs. All Rights Reserved.
//
//==============================================================================

//==============================================================================
// Include files
//===========================================================================
//#include <ansi_c.h>
#include <stdio.h>		// stdio for output
#include "visa.h"			// we need visa (typically found in your VXIPNP\include directory)
#include "DC3100_Drv.h" 	// the device driver header


//===========================================================================
// Defines
//===========================================================================
#define MY_CURRENT	0.01	// 10 mA

//===========================================================================
// Globals
//===========================================================================
ViSession 	instr 	= VI_NULL;					// instrument handle

//===========================================================================
// Prototypes
//===========================================================================
static ViStatus readInstrData(ViSession resMgr, ViRsrc rscStr, ViChar *name, ViChar *descr, ViAccessMode *lockState);
static ViStatus CleanupScan(ViSession resMgr, ViFindList findList, ViStatus err);
void error_exit(ViStatus err);
void waitKeypress(void);

//==============================================================================
// Main
//==============================================================================
int main (int argc, char *argv[])
{
	ViStatus 		err 		= VI_SUCCESS; 					// error variable
	ViSession   	resMgr	= VI_NULL;						// resource manager
	ViFindList		findList = VI_NULL; 						// find list
	ViChar*			rscPtr	= VI_NULL;						// pointer to resource string
	ViUInt32    	cnt		= 0;								// counts found devices
	ViChar			alias[DC3100_BUFFER_SIZE];				// string to copy the alias of a device to
	ViChar			name[DC3100_BUFFER_SIZE];				// string to copy the name of a device to
	ViChar			rscStr[VI_FIND_BUFLEN];					// resource string
	ViAccessMode	lockState; 									// lock state of a device

	// Welcome line
	printf("Thorlabs DC3100 instrument driver sample application\n");

	// Parameter checking / Resource scanning
   if(argc < 2)
   {
      // Find resources
      printf("Scanning for DC3100 instruments ...\n");
      if((err = viOpenDefaultRM(&resMgr))) error_exit(err);
      if((err = viFindRsrc(resMgr, DC3100_FIND_PATTERN, &findList, &cnt, rscStr))) error_exit(err);

		// Information line
		printf("Found %u port%s ...\n\n", cnt, (cnt>1) ? "s" : "");

		// Iterate all found devices
		while(cnt)
		{
			// Read information
			if(readInstrData(resMgr, rscStr, name, alias, &lockState) == VI_SUCCESS)
			{
				// Check for the devices name
				if((strstr(alias, "DC3100")))
				{
					// If found we store the device description pointer
					rscPtr = rscStr;
					CleanupScan(resMgr, findList, VI_SUCCESS);
					break;
				}
			}
			cnt --;
			if(cnt)
			{
				// Find next device
				if((err = viFindNext(findList, rscStr))) return (CleanupScan(resMgr, findList, err));
			}
		}
   }
   else
   {
      // Got resource in command line
      rscPtr = argv[1];
   }

	if(!rscPtr)
	{
		// Message that no DC3100 was found
		printf("No DC3100 found ...\n\n");

		waitKeypress();
	}
	else
	{
		// Try to open DC3100
		printf("Opening session to '%s' ...\n\n", alias);
		err = DC3100_init(rscStr, VI_OFF, VI_OFF, &instr);

		// Error handling
		if(err)	error_exit(err);

		// Switch off the LED
		err = DC3100_setLedOnOff(instr, 0);
		if(err)	error_exit(err);

		// Set the modus to constant current
		err = DC3100_setOperationMode(instr, MODUS_CONST_CURRENT);
		if(err)	error_exit(err);

		// Set the constant current
		err = DC3100_setConstCurrent(instr, MY_CURRENT);
		if(err)	error_exit(err);

		// Switch on the LED
		err = DC3100_setLedOnOff(instr, 1);
		if(err)	error_exit(err);

		// wait until a key is pressed
		waitKeypress();

		// switch off the LED
		err = DC3100_setLedOnOff(instr, 0);
		if(err)	error_exit(err);

		// Close device
		err = DC3100_close(instr);
		if(err)	error_exit(err);
	}

	// Leave main
	return err;
}


/*---------------------------------------------------------------------------
  Error exit
---------------------------------------------------------------------------*/
void error_exit(ViStatus err)
{
   ViChar ebuf[DC3100_ERR_DESCR_BUFFER_SIZE];

   // Print error
   DC3100_errorMessage(instr, err, ebuf);
   fprintf(stderr, "ERROR: %s\n", ebuf);

	// Close instrument handle if open
   if(instr != VI_NULL) DC3100_close(instr);

	// Exit program
   waitKeypress();

   exit (err);
}


/*---------------------------------------------------------------------------
  Print keypress message and wait
---------------------------------------------------------------------------*/
void waitKeypress(void)
{
   printf("Press <ENTER> to exit\n");
   while(getchar() == EOF);
}


/*---------------------------------------------------------------------------
	Read instrument data. Reads the data from the opened VISA instrument
	session.
	Return value: VISA library error code.

	Note: For Serial connections we use the VISA alias as name and the
	      'VI_ATTR_INTF_INST_NAME' as description.
---------------------------------------------------------------------------*/
static ViStatus readInstrData(ViSession resMgr, ViRsrc rscStr, ViChar *name, ViChar *descr, ViAccessMode *lockState)
{
#define DEF_VI_OPEN_TIMEOUT	1500 				// open timeout in milliseconds
#define DEF_INSTR_NAME			"Unknown"		// default device name
#define DEF_INSTR_ALIAS			""					// default alias
#define DEF_LOCK_STATE			VI_NO_LOCK		// not locked by default

	ViStatus		err;
	ViSession	instr;
	ViUInt16		intfType, intfNum;
	ViInt16     ri_state, cts_state, dcd_state;

	// Default values
	strcpy(name,  DEF_INSTR_NAME);
	strcpy(descr, DEF_INSTR_ALIAS);
	*lockState = DEF_LOCK_STATE;

	// Get alias
	if((err = viParseRsrcEx(resMgr, rscStr, &intfType, &intfNum, VI_NULL, VI_NULL, name)) < 0) return (err);
	if(intfType != VI_INTF_ASRL) return (VI_ERROR_INV_RSRC_NAME);
	// Open resource
	err = viOpen (resMgr, rscStr, VI_NULL, DEF_VI_OPEN_TIMEOUT, &instr);
   if(err == VI_ERROR_RSRC_BUSY)
   {
      // Port is open in another application
      if(strncmp(name, "LPT", 3) == 0)
      {
         // Probably we have a LPT port - do not show this
         return (VI_ERROR_INV_OBJECT);
      }
      // display port as locked
      *lockState = VI_EXCLUSIVE_LOCK ;
      return (VI_SUCCESS);
   }
   if(err) return (err);
	// Get attribute data
	err = 			viGetAttribute(instr, VI_ATTR_RSRC_LOCK_STATE, lockState);
	if(!err) err = viGetAttribute(instr, VI_ATTR_INTF_INST_NAME,  descr);
	// Get wire attributes to exclude LPT...
	if(!err) err = viGetAttribute(instr, VI_ATTR_ASRL_RI_STATE,   &ri_state);
	if(!err) err = viGetAttribute(instr, VI_ATTR_ASRL_CTS_STATE,  &cts_state);
	if(!err) err = viGetAttribute(instr, VI_ATTR_ASRL_DCD_STATE,  &dcd_state);
	if(!err)
	{
	   if((ri_state == VI_STATE_UNKNOWN) && (cts_state == VI_STATE_UNKNOWN) && (dcd_state == VI_STATE_UNKNOWN)) err = VI_ERROR_INV_OBJECT;
	}
	// Closing
	viClose(instr);
	return (err);
}


/*---------------------------------------------------------------------------
	Cleanup scan. Frees the data structures used for scanning instruments.
	Return value: value passed via the 'err' parameter.
---------------------------------------------------------------------------*/
static ViStatus CleanupScan(ViSession resMgr, ViFindList findList, ViStatus err)
{
	if(findList != VI_NULL) viClose(findList);
	if(resMgr != VI_NULL) viClose(resMgr);
	return (err);
}
